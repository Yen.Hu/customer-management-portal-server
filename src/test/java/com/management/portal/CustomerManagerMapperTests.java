package com.management.portal;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.management.portal.configuration.WebSecurityConfiguration;
import com.management.portal.domain.CustomerManager;
import com.management.portal.domain.mapper.CustomerManagerMapper;

@SpringBootTest
class CustomerManagerMapperTests {
    private static final String NAME = "〇〇テスト";
    private static final String PASSWORD = "Passw0rd123456";
    private static final Long ID = 10023L;

    @Autowired
    CustomerManagerMapper customerManagerMapper;
    @Autowired
    WebSecurityConfiguration webSecurityConfiguration;

    /*
     * Create a customer manager using name and password, user would use an auto generated ID to login the system.
     */
    @Test
    @Disabled
    void createCustomerManager_whenProvidingNameAndPassword_shouldReturnTrue() {
        assertEquals(1, customerManagerMapper.save(NAME, webSecurityConfiguration.encode(PASSWORD)));
    }

    /*
     * Spring Security Authentication, CustomerManager implements UserDetails Interface
     */
    @Test
    void findCustomerManager_whenProvidingIdString_shouldReturnCustomerManagerAndEachFieldMatches() {
        CustomerManager customerManager = customerManagerMapper.findCustomerManagerById(ID);
        assertEquals(NAME, customerManager.getName());
        assertEquals(true,
            webSecurityConfiguration.isPasswordMatchesWithEncryptedPassword(PASSWORD, customerManager.getPassword()));
    }
}
