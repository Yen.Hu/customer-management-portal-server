package com.management.portal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.management.portal.domain.CustomerManagerRelationship;
import com.management.portal.domain.mapper.CustomerManagerRelationshipMapper;

@SpringBootTest
class CustomerManagerRelationshipMapperTests {

    private static final Long MANAGER_ID = 10015L;
    private static final Long NEW_MANAGER_ID = 10016L;
    private static final Long CUSTOMER_ID = 10003L;
    private static final Logger log = LoggerFactory.getLogger(CustomerManagerRelationshipMapperTests.class);

    CustomerManagerRelationship customerManagerRelationship = new CustomerManagerRelationship();

    @Autowired
    CustomerManagerRelationshipMapper customerManagerRelationshipMapper;

    @Test
    @Order(1)
    void saveCustomerManagerRelationship_whenGivenIdsOfEach_shouldReturnSuccessInsertedCount() {
        customerManagerRelationship.setCustomerId(CUSTOMER_ID);
        customerManagerRelationship.setCustomerManagerId(MANAGER_ID);
        assertEquals(1, customerManagerRelationshipMapper.save(customerManagerRelationship));;
    }

    @Test
    @Order(2)
    void updateCustomerManagerRelationship_whenGivenIdsOfNewManagerAndCustomer_shouldReturnSuccessUpdatedCount() {
        assertEquals(1, customerManagerRelationshipMapper.update(NEW_MANAGER_ID, CUSTOMER_ID));
    }

    @Test
    @Order(3)
    void findCustomerManagerRelationship_whenGivenCustomerId_shouldOnlyReturnOneRecord() {
        customerManagerRelationship = customerManagerRelationshipMapper.findByCustomerId(CUSTOMER_ID);
        log.debug(customerManagerRelationship.toString());
        assertEquals(NEW_MANAGER_ID, customerManagerRelationship.getCustomerManagerId());
    }

    @Test
    @Order(4)
    void findCustomerManagerRelationship_whenGivenManagerId_shouldReturnZeroOrMoreRecords() {
        List<CustomerManagerRelationship> customerManagerRelationships =
            customerManagerRelationshipMapper.findByManagerId(NEW_MANAGER_ID);
        if (customerManagerRelationships.isEmpty()) {
            log.debug("Relationship Not Found");
        } else {
            log.debug(customerManagerRelationships.toString());
            for (CustomerManagerRelationship customerManagerRelationship : customerManagerRelationships) {
                log.debug(customerManagerRelationship.toString());
                assertNotNull(customerManagerRelationship.getCustomerId());
            }
        }

    }
}
