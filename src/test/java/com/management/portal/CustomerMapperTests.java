package com.management.portal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.Date;
import java.util.List;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.management.portal.domain.Customer;
import com.management.portal.domain.mapper.CustomerMapper;

@SpringBootTest
class CustomerMapperTests {
    @Autowired
    CustomerMapper customerMapper;

    // Fake Data for Creating a Customer
    private static final String NAME = "前田テスト";
    private static final Long VERSION = 1L;
    private static final Long CREATOR_ID = 10010L;
    private static final Long MODIFIER_ID = 10010L;
    private static final String FAX = "03-6758-3700";
    private static final String TELEPHONE = "0570-013904";
    private static final String WEBSITE = "https://www.moj.go.jp/isa/index.html";
    private static final String POSTOAL_CODE = "380-0846";
    private static final String ADDRESS = "長野県長野市旭町1108 長野第一合同庁舎３階";
    private static final String INDUSTRY = "金融系（銀行）";
    private static final Long HEADCOOUNT = 1000L;
    private static final String TYPE = "投資家";
    private static final Long ANNUAL_REVENUE = 1000000000L;
    private static final String BANK_NAME = "三井住友銀行";
    private static final String BRANCH_NAME = "イオンモール千葉ニュータウン店";
    private static final String ACCOUNT_NUMBER = "2394851";
    private static final Date CLOSING_DATE = Date.valueOf("2023-6-1");
    private static final String PAYMENT_TERM = "45";
    private static final String ACCOUNT_HOLDER = "取引先テスト";
    private static final String DEPOSIT_TYPE = "普通預金";
    private static final String COMMENT = "＝＝＝＝＝＝＝　テストテキスト　＝＝＝＝＝＝＝";
    private static final Integer RECENT_MODIFIED_CUSTOMERS_COUNT = 5;
    // This ID coresspond to a Simple Created Customer / Saved Customer, i.e. 吉田テスト
    private static final Long UPDATED_CUSTOMER_ID = 10002L;
    // This ID coresspond to a Manager, i.e. 高島テスト
    private static final Long MODIFIER_MANAGER_ID = 10013L;

    Customer customer = new Customer();

    private static final Logger log = LoggerFactory.getLogger(CustomerMapperTests.class);

    @Disabled
    @Test
    void createCustomer_whenOnlyProvidingPartOfFieldsValues_shouldSuccessWithDefaultValues() {
        // Non-null Fields
        customer.setName(NAME);
        customer.setVersion(VERSION);
        customer.setCreatorId(CREATOR_ID);
        customer.setModifierId(MODIFIER_ID);

        // Optional or Save & Edit Later
        customer.setFax(FAX);
        customer.setTelephone(TELEPHONE);

        assertEquals(true, customerMapper.save(customer));
    }

    @Disabled
    @Test
    void createCustomer_whenFullyProvidingFieldsValues_shouldSuccessAndWithDBDefaultValues() {
        customer.setName(NAME);
        customer.setVersion(VERSION);
        customer.setCreatorId(CREATOR_ID);
        customer.setModifierId(MODIFIER_ID);
        customer.setFax(FAX);
        customer.setTelephone(TELEPHONE);
        customer.setWebsite(WEBSITE);
        customer.setPostalCode(POSTOAL_CODE);
        customer.setAddress(ADDRESS);
        customer.setIndustry(INDUSTRY);
        customer.setHeadcount(HEADCOOUNT);
        customer.setType(TYPE);
        customer.setAnnualRevenue(ANNUAL_REVENUE);
        customer.setBankName(BANK_NAME);
        customer.setBranchName(BRANCH_NAME);
        customer.setAccountNumber(ACCOUNT_NUMBER);
        customer.setAccountHolder(ACCOUNT_HOLDER);
        customer.setClosingDate(CLOSING_DATE);
        customer.setPaymentTerm(PAYMENT_TERM);
        customer.setDepositType(DEPOSIT_TYPE);
        customer.setComment(COMMENT);
        assertEquals(true, customerMapper.save(customer));
    }

    @Test
    @Disabled
    void updateCustomers_whenGivenPartOfFieldsValues_shouldAlsoUpdateModifiedTimeModiferIdAndVersion() {
        customer.setId(UPDATED_CUSTOMER_ID);
        customer.setModifierId(MODIFIER_MANAGER_ID);
        customer.setBankName(BANK_NAME);
        assertTrue(customerMapper.update(customer) > 0);
    }

    @Test
    void findCustomers_whenModifiedOrCreatedRecently_shouldReturnTopFiveOrLessThanFiveModifiedCustomers() {
        List<Customer> modifiedOrCreatedRecentlyCustomers = customerMapper.findTopFiveRecentlyModifiedCustomers();

        log.debug("Printing Results: ");
        for (Customer customer : modifiedOrCreatedRecentlyCustomers) {
            log.debug(customer.toString());
        }

        boolean isDecreasing = true;
        for (int i = 1; i < modifiedOrCreatedRecentlyCustomers.size(); i++) {
            if (modifiedOrCreatedRecentlyCustomers.get(i).getModifiedTime()
                .compareTo(modifiedOrCreatedRecentlyCustomers.get(i - 1).getModifiedTime()) >= 0) {
                isDecreasing = false;
                break;
            }
        }

        assertTrue(modifiedOrCreatedRecentlyCustomers.size() <= RECENT_MODIFIED_CUSTOMERS_COUNT && isDecreasing,
            "Should show less than 5 recently modified customers.");
    }

}
