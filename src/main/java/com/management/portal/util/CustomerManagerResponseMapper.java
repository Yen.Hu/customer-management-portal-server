package com.management.portal.util;

import com.management.portal.domain.CustomerManager;
import com.management.portal.domain.CustomerManagerResponse;

public class CustomerManagerResponseMapper {
    private CustomerManagerResponseMapper() {
        throw new IllegalStateException("Utility class");
    }

    public static CustomerManagerResponse toResponse(CustomerManager customerManager) {
        return new CustomerManagerResponse(customerManager.getId(), customerManager.getName());
    }
}
