package com.management.portal.domain;

import java.io.Serializable;

public class CustomerManagerResponse implements Serializable {
    private static final long serialVersionUID = -7684672181756954451L;
    private Long id;
    private String name;

    public CustomerManagerResponse() {}

    public CustomerManagerResponse(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "CustomerManagerResponse [id=" + id + ", name=" + name + "]";
    }
}
