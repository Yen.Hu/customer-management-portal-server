package com.management.portal.domain;

import java.io.Serializable;

public class CustomerManagerRelationship implements Serializable {
    private static final long serialVersionUID = -3070922253614289561L;
    private Long customerId;
    private Long customerManagerId;

    public CustomerManagerRelationship() {}

    public CustomerManagerRelationship(Long customerId, Long customerManagerId) {
        this.customerId = customerId;
        this.customerManagerId = customerManagerId;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Long getCustomerManagerId() {
        return customerManagerId;
    }

    public void setCustomerManagerId(Long customerManagerId) {
        this.customerManagerId = customerManagerId;
    }

    @Override
    public String toString() {
        return "CustomerManagerRelationship [customerId=" + customerId + ", customerManagerId=" + customerManagerId
            + "]";
    }

}
