package com.management.portal.domain;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

public class Customer implements Serializable {
    private static final long serialVersionUID = -6413958998373373914L;
    private Long id;
    private String name;
    private String fax;
    private String telephone;
    private String website;
    private String postalCode;
    private String address;
    private String industry;
    private String type;
    private String bankName;
    private String branchName;
    private String accountNumber;
    private String paymentTerm;
    private String accountHolder;
    private String depositType;
    private String comment;
    private Date closingDate;
    private Long version;
    private Long headcount;
    private Long annualRevenue;
    private Long creatorId;
    private Long modifierId;
    private Timestamp createdTime;
    private Timestamp modifiedTime;

    public Customer() {}

    public Customer(Long id, String name, String fax, String telephone, String website, String postalCode,
        String address, String industry, String type, String bankName, String branchName, String accountNumber,
        String paymentTerm, String accountHolder, String depositType, String comment, Date closingDate, Long version,
        Long headcount, Long annualRevenue, Long creatorId, Long modifierId, Timestamp createdTime,
        Timestamp modifiedTime) {
        this.id = id;
        this.name = name;
        this.fax = fax;
        this.telephone = telephone;
        this.website = website;
        this.postalCode = postalCode;
        this.address = address;
        this.industry = industry;
        this.type = type;
        this.bankName = bankName;
        this.branchName = branchName;
        this.accountNumber = accountNumber;
        this.paymentTerm = paymentTerm;
        this.accountHolder = accountHolder;
        this.depositType = depositType;
        this.comment = comment;
        this.closingDate = closingDate;
        this.version = version;
        this.headcount = headcount;
        this.annualRevenue = annualRevenue;
        this.creatorId = creatorId;
        this.modifierId = modifierId;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getPaymentTerm() {
        return paymentTerm;
    }

    public void setPaymentTerm(String paymentTerm) {
        this.paymentTerm = paymentTerm;
    }

    public String getAccountHolder() {
        return accountHolder;
    }

    public void setAccountHolder(String accountHolder) {
        this.accountHolder = accountHolder;
    }

    public String getDepositType() {
        return depositType;
    }

    public void setDepositType(String depositType) {
        this.depositType = depositType;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getClosingDate() {
        return closingDate;
    }

    public void setClosingDate(Date closingDate) {
        this.closingDate = closingDate;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Long getHeadcount() {
        return headcount;
    }

    public void setHeadcount(Long headcount) {
        this.headcount = headcount;
    }

    public Long getAnnualRevenue() {
        return annualRevenue;
    }

    public void setAnnualRevenue(Long annualRevenue) {
        this.annualRevenue = annualRevenue;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    public Long getModifierId() {
        return modifierId;
    }

    public void setModifierId(Long modifierId) {
        this.modifierId = modifierId;
    }

    public Timestamp getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Timestamp createdTime) {
        this.createdTime = createdTime;
    }

    public Timestamp getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(Timestamp modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    @Override
    public String toString() {
        return "Customer [id=" + id + ", name=" + name + ", fax=" + fax + ", telephone=" + telephone + ", website="
            + website + ", postalCode=" + postalCode + ", address=" + address + ", industry=" + industry + ", type="
            + type + ", bankName=" + bankName + ", branchName=" + branchName + ", accountNumber=" + accountNumber
            + ", paymentTerm=" + paymentTerm + ", accountHolder=" + accountHolder + ", depositType=" + depositType
            + ", comment=" + comment + ", closingDate=" + closingDate + ", version=" + version + ", headcount="
            + headcount + ", annualRevenue=" + annualRevenue + ", creatorId=" + creatorId + ", modifierId=" + modifierId
            + ", createdTime=" + createdTime + ", modifiedTime=" + modifiedTime + "]";
    }

}
