package com.management.portal.domain.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.management.portal.domain.Customer;

/**
 * 
 * @author Yan Hu 取引先 Mapper
 */
@Mapper
public interface CustomerMapper {
    /*
     * Dynamic SQL for insert part/all customer fields' value into DB
     * Used for Save, Simple Customer Creation and Customer Creation
     */
    @Options(useGeneratedKeys = true, keyProperty = "id")
    @Insert({"<script>", "INSERT INTO Customer(" + "<if test='name != null'>name,</if>"
        + "<if test='fax != null'>fax,</if>" + "<if test='telephone != null'>telephone,</if>"
        + "<if test='website != null'>website,</if>" + "<if test='postalCode != null'>postal_code,</if>"
        + "<if test='address != null'>address,</if>" + "<if test='industry != null'>industry,</if>"
        + "<if test='type != null'>type,</if>" + "<if test='bankName != null'>bank_name,</if>"
        + "<if test='branchName != null'>branch_name,</if>" + "<if test='accountNumber != null'>account_number,</if>"
        + "<if test='paymentTerm != null'>payment_term,</if>" + "<if test='accountHolder != null'>account_holder,</if>"
        + "<if test='depositType != null'>deposit_type,</if>" + "<if test='comment != null'>comment,</if>"
        + "<if test='closingDate != null'>closing_date,</if>" + "<if test='version != null'>version,</if>"
        + "<if test='headcount != null'>headcount,</if>" + "<if test='annualRevenue != null'>annual_revenue,</if>"
        + "<if test='creatorId != null'>creator_id,</if>" + "<if test='modifierId != null'>modifier_id</if>"
        + "  ) VALUES (" + "<if test='name != null'>#{name},</if>" + "<if test='fax != null'>#{fax},</if>"
        + "<if test='telephone != null'>#{telephone},</if>" + "<if test='website != null'>#{website},</if>"
        + "<if test='postalCode != null'>#{postalCode},</if>" + "<if test='address != null'>#{address},</if>"
        + "<if test='industry != null'>#{industry},</if>" + "<if test='type != null'>#{type},</if>"
        + "<if test='bankName != null'>#{bankName},</if>" + "<if test='branchName != null'>#{branchName},</if>"
        + "<if test='accountNumber != null'>#{accountNumber},</if>"
        + "<if test='paymentTerm != null'>#{paymentTerm},</if>"
        + "<if test='accountHolder != null'>#{accountHolder},</if>"
        + "<if test='depositType != null'>#{depositType},</if>" + "<if test='comment != null'>#{comment},</if>"
        + "<if test='closingDate != null'>#{closingDate},</if>" + "<if test='version != null'>#{version},</if>"
        + "<if test='headcount != null'>#{headcount},</if>" + "<if test='annualRevenue != null'>#{annualRevenue},</if>"
        + "<if test='creatorId != null'>#{creatorId},</if>" + "<if test='modifierId != null'>#{modifierId}</if>" + ")"
        + "</script>"})
    Integer save(Customer c);

    @Update({"<script>", "update Customer", "  <set>", "    modified_time=CURRENT_TIMESTAMP,",
        "    <if test='name != null'>name=#{name},</if>", "    <if test='fax != null'>fax=#{fax},</if>",
        "    <if test='telephone != null'>telephone=#{telephone},</if>",
        "    <if test='website != null'>website=#{website},</if>",
        "    <if test='postalCode != null'>postal_code=#{postalCode},</if>",
        "    <if test='address != null'>address=#{address},</if>",
        "    <if test='industry != null'>industry=#{industry},</if>", "    <if test='type != null'>type=#{type},</if>",
        "    <if test='bankName != null'>bank_name=#{bankName},</if>",
        "    <if test='branchName != null'>branch_name=#{branchName},</if>",
        "    <if test='accountNumber != null'>account_number=#{accountNumber},</if>",
        "    <if test='paymentTerm != null'>payment_term=#{paymentTerm},</if>",
        "    <if test='accountHolder != null'>account_holder=#{accountHolder},</if>",
        "    <if test='depositType != null'>deposit_type=#{depositType},</if>",
        "    <if test='comment != null'>comment=#{comment},</if>",
        "    <if test='version == null'>version=version+1,</if>",
        "    <if test='headcount != null'>headcount=#{headcount},</if>",
        "    <if test='annualRevenue != null'>annual_revenue=#{annualRevenue},</if>",
        "    <if test='modifierId != null'>modifier_id=#{modifierId},</if>",
        "    <if test='closingDate != null'>closing_date=#{closingDate},</if>", "  </set>", "where id=#{id}",
        "</script>"})
    Integer update(Customer customer);

    @Select("SELECT * FROM Customer WHERE id = #{id}")
    @Result(property = "id", column = "id", id = true)
    @Result(property = "createdTime", column = "created_time")
    @Result(property = "modifiedTime", column = "modified_time")
    @Result(property = "name", column = "name")
    @Result(property = "fax", column = "fax")
    @Result(property = "telephone", column = "telephone")
    @Result(property = "website", column = "website")
    @Result(property = "postalCode", column = "postal_code")
    @Result(property = "address", column = "address")
    @Result(property = "industry", column = "industry")
    @Result(property = "type", column = "type")
    @Result(property = "bankName", column = "bank_name")
    @Result(property = "branchName", column = "branch_name")
    @Result(property = "accountNumber", column = "account_number")
    @Result(property = "paymentTerm", column = "payment_term")
    @Result(property = "accountHolder", column = "account_holder")
    @Result(property = "depositType", column = "deposit_type")
    @Result(property = "comment", column = "comment")
    @Result(property = "version", column = "version")
    @Result(property = "headcount", column = "headcount")
    @Result(property = "annualRevenue", column = "annual_revenue")
    @Result(property = "creatorId", column = "creator_id")
    @Result(property = "modifierId", column = "modifier_id")
    @Result(property = "closingDate", column = "closing_date")
    Customer findCustomerById(@Param("id") Long id);

    @Results({@Result(property = "id", column = "id", id = true),
        @Result(property = "createdTime", column = "created_time"),
        @Result(property = "modifiedTime", column = "modified_time"), @Result(property = "name", column = "name"),
        @Result(property = "fax", column = "fax"), @Result(property = "telephone", column = "telephone"),
        @Result(property = "website", column = "website"), @Result(property = "postalCode", column = "postal_code"),
        @Result(property = "address", column = "address"), @Result(property = "industry", column = "industry"),
        @Result(property = "type", column = "type"), @Result(property = "bankName", column = "bank_name"),
        @Result(property = "branchName", column = "branch_name"),
        @Result(property = "accountNumber", column = "account_number"),
        @Result(property = "paymentTerm", column = "payment_term"),
        @Result(property = "accountHolder", column = "account_holder"),
        @Result(property = "depositType", column = "deposit_type"), @Result(property = "comment", column = "comment"),
        @Result(property = "version", column = "version"), @Result(property = "headcount", column = "headcount"),
        @Result(property = "annualRevenue", column = "annual_revenue"),
        @Result(property = "creatorId", column = "creator_id"),
        @Result(property = "modifierId", column = "modifier_id"),
        @Result(property = "closingDate", column = "closing_date")})
    @Select("SELECT * FROM Customer ORDER BY modified_time DESC LIMIT 5")
    List<Customer> findTopFiveRecentlyModifiedCustomers();
}
