package com.management.portal.domain.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.management.portal.domain.CustomerManagerRelationship;

@Mapper
public interface CustomerManagerRelationshipMapper {
    @Select("SELECT * FROM CustomerManager WHERE manager_id = #{managerId}")
    @Result(property = "customerId", column = "customer_id", id = true)
    @Result(property = "customerManagerId", column = "manager_id")
    List<CustomerManagerRelationship> findByManagerId(@Param("managerId") Long managerId);

    @Select("SELECT * FROM CustomerManager WHERE customer_id = #{customerId}")
    @Result(property = "customerId", column = "customer_id", id = true)
    @Result(property = "customerManagerId", column = "manager_id")
    CustomerManagerRelationship findByCustomerId(@Param("customerId") Long customerId);

    @Insert("INSERT INTO CustomerManager(manager_id, customer_id) VALUES (#{customerManagerId}, #{customerId})")
    Integer save(CustomerManagerRelationship customersManagerRelationship);

    @Update("UPDATE CustomerManager SET manager_id = #{managerId} WHERE customer_id = #{customerId}")
    Integer update(@Param("managerId") Long managerId, @Param("customerId") Long customerId);

    @Delete("DELETE FROM CustomerManager WHERE manager_id = #{managerId} AND customer_id = #{customerId}")
    Integer delete(@Param("managerId") Long managerId, @Param("customerId") Long customerId);
}