package com.management.portal.domain.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;

import com.management.portal.domain.Customer;

/**
 * 
 * @author Yan Hu 取引先 Mapper
 */
@Mapper
public interface CustomerHistoryMapper {
    @Insert({"<script>", "INSERT INTO CustomerHistory(" + "<if test='id != null'>id,</if>"
        + "<if test='name != null'>name,</if>" + "<if test='fax != null'>fax,</if>"
        + "<if test='telephone != null'>telephone,</if>" + "<if test='website != null'>website,</if>"
        + "<if test='postalCode != null'>postal_code,</if>" + "<if test='address != null'>address,</if>"
        + "<if test='industry != null'>industry,</if>" + "<if test='type != null'>type,</if>"
        + "<if test='bankName != null'>bank_name,</if>" + "<if test='branchName != null'>branch_name,</if>"
        + "<if test='accountNumber != null'>account_number,</if>" + "<if test='paymentTerm != null'>payment_term,</if>"
        + "<if test='accountHolder != null'>account_holder,</if>" + "<if test='depositType != null'>deposit_type,</if>"
        + "<if test='comment != null'>comment,</if>" + "<if test='closingDate != null'>closing_date,</if>"
        + "<if test='version != null'>version,</if>" + "<if test='headcount != null'>headcount,</if>"
        + "<if test='annualRevenue != null'>annual_revenue,</if>" + "<if test='creatorId != null'>creator_id,</if>"
        + "<if test='modifierId != null'>modifier_id</if>" + "  ) VALUES (" + "<if test='id != null'>#{id},</if>"
        + "<if test='name != null'>#{name},</if>" + "<if test='fax != null'>#{fax},</if>"
        + "<if test='telephone != null'>#{telephone},</if>" + "<if test='website != null'>#{website},</if>"
        + "<if test='postalCode != null'>#{postalCode},</if>" + "<if test='address != null'>#{address},</if>"
        + "<if test='industry != null'>#{industry},</if>" + "<if test='type != null'>#{type},</if>"
        + "<if test='bankName != null'>#{bankName},</if>" + "<if test='branchName != null'>#{branchName},</if>"
        + "<if test='accountNumber != null'>#{accountNumber},</if>"
        + "<if test='paymentTerm != null'>#{paymentTerm},</if>"
        + "<if test='accountHolder != null'>#{accountHolder},</if>"
        + "<if test='depositType != null'>#{depositType},</if>" + "<if test='comment != null'>#{comment},</if>"
        + "<if test='closingDate != null'>#{closingDate},</if>" + "<if test='version != null'>#{version},</if>"
        + "<if test='headcount != null'>#{headcount},</if>" + "<if test='annualRevenue != null'>#{annualRevenue},</if>"
        + "<if test='creatorId != null'>#{creatorId},</if>" + "<if test='modifierId != null'>#{modifierId}</if>" + ")"
        + "</script>"})
    Integer save(Customer c);

}
