package com.management.portal.domain.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Select;

import com.management.portal.domain.CustomerManager;

@Mapper
public interface CustomerManagerMapper {

    @Insert("INSERT INTO GeneralMaster (name, encrypted_password) VALUES(#{name}, #{password})")
    Integer save(String name, String password);

    @Select("SELECT * FROM GeneralMaster WHERE id = #{customerManagerId}")
    @Result(property = "id", column = "id", id = true)
    @Result(property = "name", column = "name")
    @Result(property = "password", column = "encrypted_password")
    @Result(property = "createdTime", column = "created_time")
    @Result(property = "modifiedTime", column = "modified_time")
    CustomerManager findCustomerManagerById(Long customerManagerId);
}
