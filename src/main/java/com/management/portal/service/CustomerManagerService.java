package com.management.portal.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.management.portal.domain.CustomerManager;
import com.management.portal.domain.mapper.CustomerManagerMapper;
import com.management.portal.exception.NoAuthenticatedUserFoundException;

@Service
public class CustomerManagerService {
    @Autowired
    CustomerManagerMapper customerManagerMapper;
    @Autowired
    AuthUserService authUserService;

    public CustomerManager findCurrentSessionCustomerManager() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.getPrincipal() instanceof CustomerManager) {
            return (CustomerManager)authentication.getPrincipal();
        } else {
            throw new NoAuthenticatedUserFoundException("No Auth User Found");
        }
    }
    
    public String findCustomerManagerNameById(Long id) {
        return customerManagerMapper.findCustomerManagerById(id).getName();
    }
}
