package com.management.portal.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.management.portal.domain.CustomerManagerRelationship;
import com.management.portal.domain.mapper.CustomerManagerRelationshipMapper;

@Service
public class CustomerManagerRelationshipService {
    @Autowired
    CustomerManagerRelationshipMapper customerManagerRelationshipMapper;

    @Autowired
    CustomerManagerService customerManagerService;

    public Integer save(Long customerId, Long customerManagerId) {
        CustomerManagerRelationship customerManagerRelationship = new CustomerManagerRelationship();
        customerManagerRelationship.setCustomerId(customerId);
        customerManagerRelationship.setCustomerManagerId(customerManagerId);
        return customerManagerRelationshipMapper.save(customerManagerRelationship);
    }

    public String findCustomerManagerNameByCustomerId(Long id) {
        return customerManagerService
            .findCustomerManagerNameById(customerManagerRelationshipMapper.findByCustomerId(id).getCustomerManagerId());
    }
}
