package com.management.portal.service;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.management.portal.exception.NoAuthenticatedUserFoundException;

@Service
public class AuthUserService {
    public Object getCurrentAuthenticatedUserPrincipal() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            return authentication.getPrincipal();
        }
        throw new NoAuthenticatedUserFoundException("No Auth User Found");
    }
}
