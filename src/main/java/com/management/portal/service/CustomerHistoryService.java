package com.management.portal.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.management.portal.domain.Customer;
import com.management.portal.domain.mapper.CustomerHistoryMapper;

@Service
public class CustomerHistoryService {
    @Autowired
    CustomerHistoryMapper customerHistoryMapper;

    public Integer save(Customer customer) {
        return customerHistoryMapper.save(customer);
    }

}
