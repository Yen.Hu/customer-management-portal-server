package com.management.portal.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.management.portal.domain.CustomerManager;
import com.management.portal.domain.mapper.CustomerManagerMapper;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    CustomerManagerMapper customerManagerMapper;

    @Override
    public UserDetails loadUserByUsername(String customerManagerId) throws UsernameNotFoundException {
        CustomerManager customerManager =
            customerManagerMapper.findCustomerManagerById(Long.parseLong(customerManagerId));
        if (customerManager == null) {
            throw new UsernameNotFoundException("User: " + customerManagerId + " Not Found!");
        }
        return customerManager;
    }
}
