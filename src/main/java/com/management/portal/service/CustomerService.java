package com.management.portal.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.management.portal.domain.Customer;
import com.management.portal.domain.mapper.CustomerMapper;

@Service
public class CustomerService {
    @Autowired
    CustomerMapper customerMapper;
    @Autowired
    AuthUserService authUserService;
    @Autowired
    CustomerManagerRelationshipService customerManagerRelationshipService;
    @Autowired
    CustomerHistoryService customerHistoryService;

    public Long save(Customer customer) {
        customerMapper.save(customer);
        customerManagerRelationshipService.save(customer.getId(), customer.getCreatorId());
        customerHistoryService.save(customer);
        return customer.getId();
    }

    public Customer findCustomerById(Long id) {
        return customerMapper.findCustomerById(id);
    }

    public List<Customer> getRecentCustomers() {
        return customerMapper.findTopFiveRecentlyModifiedCustomers();
    }

    public Customer updateCustomerAndReturnUpdated(Customer updatedCustomer) {
        Customer oldCustomer = customerMapper.findCustomerById(updatedCustomer.getId());
        customerHistoryService.save(oldCustomer);
        customerMapper.update(updatedCustomer);
        updatedCustomer = customerMapper.findCustomerById(updatedCustomer.getId());
        return updatedCustomer;
    }
}
