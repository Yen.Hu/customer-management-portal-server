package com.management.portal.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.management.portal.domain.Customer;
import com.management.portal.service.CustomerService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "取引先機能")
@RestController
@RequestMapping("/api/v1/customers")
public class CustomerController {
    @Autowired
    CustomerService customerService;

    @Operation(summary = "新しい取引先を新規作成する",
        description = "このAPIは、取引先新規画面や取引先ポータル画面で呼び出されます。操作により履歴データと取引先と担当者の関係データも追加されます。")
    @PostMapping
    public Long createCustomer(@RequestBody Customer customer) {
        return customerService.save(customer);
    }

    @Operation(summary = "IDで取引先データを取得する", description = "このAPIは、取引先詳細画面で呼び出されます。")
    @GetMapping("/{id}")
    public Customer getCustomerById(@PathVariable @Parameter(description = "取引先ID") Long id) {
        return customerService.findCustomerById(id);
    }

    @Operation(summary = "新しい取引先データで取引先データを更新する", description = "このAPIは、取引先編集・更新画面で呼び出されます。操作により履歴レコードも追加されます。")
    @PutMapping("/{id}")
    public Customer updateCustomer(@PathVariable @Parameter(description = "取引先ID") Long id, @RequestBody Customer customer) {
        customer.setId(id);
        return customerService.updateCustomerAndReturnUpdated(customer);
    }

    @Operation(summary = "最近の取引先データを取得する", description = "このAPIは、取引先ポータル画面で呼び出されます。最近更新された取引先データが最大5件まで表示されます。")
    @GetMapping("/recent")
    public List<Customer> getRecentCustomers() {
        return customerService.getRecentCustomers();
    }
}
