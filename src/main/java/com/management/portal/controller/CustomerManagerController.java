package com.management.portal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.management.portal.domain.CustomerManagerResponse;
import com.management.portal.service.CustomerManagerService;
import com.management.portal.util.CustomerManagerResponseMapper;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "取引先担当者機能")
@RestController
@RequestMapping("/api/v1/managers")
public class CustomerManagerController {
    @Autowired
    CustomerManagerService customerManagerService;

    @Operation(summary = "現在のセッションの取引先担当者データを取得する",
        description = "このAPIは、取引先データを更新/新規作成する際に呼び出されます。パスワードと権限フィールドはレスポンスから削除されました。")
    @GetMapping("/current")
    public CustomerManagerResponse getCustomerResponse() {
        return CustomerManagerResponseMapper.toResponse(customerManagerService.findCurrentSessionCustomerManager());
    }

    @Operation(summary = "IDで取引先担当者の名前を取得する")
    @GetMapping("/{id}")
    public String getCustomerManagerNameById(@PathVariable @Parameter(description = "取引先担当者ID") Long id) {
        return customerManagerService.findCustomerManagerNameById(id);
    }
}
