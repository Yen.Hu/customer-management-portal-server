package com.management.portal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.management.portal.service.CustomerManagerRelationshipService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "取引先と取引先担当者の関係")
@RestController
@RequestMapping("/api/v1/relationships")
public class CustomerManagerRelationshipController {
    @Autowired
    CustomerManagerRelationshipService customerManagerRelationshipService;

    @Operation(summary = "取引先のIDで取引先担当者の名前を取得する")
    @GetMapping("/customers/{id}")
    public String getCustomerManagerNameById(@PathVariable @Parameter(description = "取引先ID") Long id) {
        return customerManagerRelationshipService.findCustomerManagerNameByCustomerId(id);
    }
}
