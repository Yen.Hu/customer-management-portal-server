package com.management.portal.exception;

public class NoAuthenticatedUserFoundException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public NoAuthenticatedUserFoundException(String message) {
        super(message);
    }

}
