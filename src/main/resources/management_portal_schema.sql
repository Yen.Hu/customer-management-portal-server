-- MySQL dump 10.13  Distrib 8.0.32, for Linux (x86_64)
--
-- Host: localhost    Database: ManagementPortal
-- ------------------------------------------------------
-- Server version	8.0.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Customer`
--

DROP TABLE IF EXISTS `Customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Customer` (
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT '取引先の識別番号',
  `name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '取引先の名前',
  `fax` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'ファックス番号',
  `telephone` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '電話番号',
  `website` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'ウェブサイト',
  `version` int unsigned NOT NULL DEFAULT '0' COMMENT '取引先データを更新する前に履歴データテーブルへコーピーする、そして version + 1',
  `postal_code` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '郵便番号',
  `address` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '住所',
  `industry` enum('金融系（銀行）','金融系（証券）','金融系（生損保）','金融系（その他）','流通','製造','組込み・制御','官公庁','その他') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '得意分野',
  `headcount` int unsigned DEFAULT NULL COMMENT '従業員数',
  `type` enum('アナリスト','競合','顧客','インテグレーター（統合）','投資家','パートナー','報道機関','見込顧客','代理店','その他') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '取引先の種別',
  `annual_revenue` int unsigned DEFAULT NULL COMMENT '年間売上',
  `bank_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '銀行名',
  `branch_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '支店名',
  `account_number` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '口座番号',
  `closing_date` datetime DEFAULT NULL COMMENT '締め日',
  `payment_term` enum('45','60','90') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '支払いサイト',
  `account_holder` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '名義人',
  `deposit_type` enum('普通預金','当座預金','貯蓄預金','その他') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '預金種別',
  `comment` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci COMMENT '備考',
  `modified_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'データ更新の時点',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'データ追加の時点',
  `creator_id` int unsigned NOT NULL COMMENT '作成者の識別番号',
  `modifier_id` int unsigned NOT NULL COMMENT '最終更新者の識別番号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10024 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CustomerHistory`
--

DROP TABLE IF EXISTS `CustomerHistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CustomerHistory` (
  `id` int unsigned NOT NULL COMMENT '取引先の識別番号',
  `name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '取引先の名前',
  `fax` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'ファックス番号',
  `telephone` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '電話番号',
  `website` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT 'ウェブサイト',
  `version` int unsigned NOT NULL DEFAULT '0' COMMENT '取引先データを更新する前に履歴データテーブルへコーピーする、そして version + 1',
  `postal_code` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '郵便番号',
  `address` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '住所',
  `industry` enum('金融系（銀行）','金融系（証券）','金融系（生損保）','金融系（その他）','流通','製造','組込み・制御','官公庁','その他') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '得意分野',
  `headcount` int unsigned DEFAULT NULL COMMENT '従業員数',
  `type` enum('アナリスト','競合','顧客','インテグレーター（統合）','投資家','パートナー','報道機関','見込顧客','代理店','その他') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '取引先の種別',
  `annual_revenue` int unsigned DEFAULT NULL COMMENT '年間売上',
  `bank_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '銀行名',
  `branch_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '支店名',
  `account_number` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '口座番号',
  `closing_date` datetime DEFAULT NULL COMMENT '締め日',
  `payment_term` enum('45','60','90') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '支払いサイト',
  `account_holder` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '名義人',
  `deposit_type` enum('普通預金','当座預金','貯蓄預金','その他') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '預金種別',
  `comment` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci COMMENT '備考',
  `modified_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'データ更新の時点',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'データ追加の時点',
  `creator_id` int unsigned NOT NULL COMMENT '作成者の識別番号',
  `modifier_id` int unsigned NOT NULL COMMENT '最終更新者の識別番号'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CustomerManager`
--

DROP TABLE IF EXISTS `CustomerManager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CustomerManager` (
  `manager_id` int unsigned NOT NULL COMMENT '取引先担当者の識別番号',
  `customer_id` int unsigned NOT NULL COMMENT '取引先の識別番号',
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `GeneralMaster`
--

DROP TABLE IF EXISTS `GeneralMaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `GeneralMaster` (
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT '取引先担当者の識別番号',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'データ追加の時点',
  `modified_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'データ更新の時点',
  `name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '取引先担当者の名前',
  `encrypted_password` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '取引先担当者のパスワード',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10023 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-07-03 21:00:36
